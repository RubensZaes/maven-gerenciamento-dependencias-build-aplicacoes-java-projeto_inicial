package loja;

import java.math.BigDecimal;

import org.junit.Test;

import br.com.alura.loja.Produto;

public class ProdutoTeste {

	@Test
	public void test() {
		Produto p = new Produto("Teste", BigDecimal.TEN);
		org.junit.Assert.assertEquals("Teste", p.getNome());
		org.junit.Assert.assertEquals(BigDecimal.TEN, p.getPreco());
	}

}
